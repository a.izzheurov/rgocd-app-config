#!/bin/bash

minikube --profile fame config set memory 8192
minikube --profile fame config set cpus 4
minikube --profile fame config set vm-driver virtualbox
minikube start --profile fame
minikube --profile fame config view
minikube --profile fame status
minikube --profile fame ip
# minikube --profile fame dashboard --url


eval $(minikube --profile fame docker-env)